/**
 * Domaća zadaća 3.1
 * Copyright (C) 2016  Kristijan Lenković
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package dz3;

import java.io.*;
import java.util.Arrays;

/**
 * - Izradite jednostavnu komandno-linijsku aplikaciju koja:
 *    - otvara txt datoteku kao tok znakova (ime datoteke neka bude
 *      primljeno kao prvi argument main metode)
 *    - čita liniju po liniju i sadržaj ispisuje:
 *      - (a) u drugu novi txt datoteku (ime neka bude primljeno kao drugi
 *        argument main metode) i
 *      - (b) u konzolu (samo ako je prisutan treći arg "-p" ili "-print")
 *    - i na kraju ispisuje poruku o uspješnosti
 *  - Program na kraju zapakirati kao JAR datoteku za distribuciju
 * 
 * @author Kristijan Lenković <k.lenkovic@me.com>
 */
public class Dz3 {

    /**
     * @param args the command line arguments
     */
    
    public static boolean   ispis;
    public static String    pathCitanje;
    public static String    pathPisanje;
    
    public static FileInputStream   datCitanje;
    public static FileOutputStream  datPisanje;
    public static BufferedReader    bufferCitanje;
    public static PrintStream       printPisanje;
    
    public static void main(String[] args) throws IOException {
        //System.out.println("Tekst");
        
        ispis = false;
        String linija;
        
        argumenti(args);
        
        //System.out.println("Ucitavam datoteku iz: " + pathCitanje);
        //System.out.println("Zapisujem u datoteku: " + pathPisanje);
        
        try{
            datCitanje = new FileInputStream(pathCitanje);
            datPisanje = new FileOutputStream(pathPisanje);
        }
        catch (FileNotFoundException e){
            System.out.println(e.getMessage());
            System.exit(1);
        }
        
        bufferCitanje = new BufferedReader(new InputStreamReader(datCitanje));
        printPisanje = new PrintStream(datPisanje);
        
        if(bufferCitanje.ready()){
            while ((linija=bufferCitanje.readLine()) != null) { 
                printPisanje.println(linija);
                if(ispis)
                    System.out.println(linija);
            }	
        }else{
            System.out.println("Datoteka nije spremna za citanje.");
            System.exit(1);
        }
        
        bufferCitanje.close();
        printPisanje.close();
        
        System.out.println("Program uspjesno izvrsen.");
        
    }
    
    private static void argumenti(String[] args) {
        String[] print = {"-p", "-print", "--print"};
        
        if(args.length < 2){
            System.out.println("Nisu uneseni obavezni parametri");
            System.exit(1);
        }
        if(args.length > 2 && Arrays.asList(print).contains(args[2]))
            ispis = true;
        
        pathCitanje = args[0];
        pathPisanje = args[1];
    }
    
}
